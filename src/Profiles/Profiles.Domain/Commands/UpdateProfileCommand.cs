using MediatR;

namespace Profiles.Domain.Commands
{
    public class UpdateProfileCommand : IRequest
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? DepartmentId { get; set; }
    }
}