using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Profiles.Domain.Events;
using Profiles.Domain.Events.Dto;
using Profiles.Domain.ProfilesAggregate;

namespace Profiles.Domain.Commands
{
    public class UpdateProfileCommandHandler : IRequestHandler<UpdateProfileCommand>
    {

        private readonly IProfilesRepository _repository;

        private readonly IEventBus _eventBus;

        public UpdateProfileCommandHandler(IProfilesRepository repository, IEventBus eventBus)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<Unit> Handle(UpdateProfileCommand request, CancellationToken cancellationToken)
        {
            var profile = new Profile 
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            if (request.DepartmentId.HasValue) 
            {
                profile.Department = new Department{ Id = request.DepartmentId.Value };
            }

            await _repository.SaveProfileAsync(profile);

            profile = await _repository.GetByIdAsync(request.Id);

            var @event = new ProfileUpdatedEvent(profile);

            _eventBus.Publish(@event);

            return new MediatR.Unit();
        }
    }
}