using Newtonsoft.Json;
using Profiles.Domain.ProfilesAggregate;

namespace Profiles.Domain.Events.Dto
{
    public class DepartmentDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        public DepartmentDTO(Department department)
        {
            this.Id = department.Id;
            this.Title = department.Title;
        }
    }
}