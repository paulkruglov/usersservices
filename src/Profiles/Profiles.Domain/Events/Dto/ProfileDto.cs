using Newtonsoft.Json;
using Profiles.Domain.ProfilesAggregate;

namespace Profiles.Domain.Events.Dto
{
    public class ProfileDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("department")]
        public DepartmentDTO Department { get; set; }

        public ProfileDTO(Profile profile)
        {
            this.Id = profile.Id;
            this.FirstName = profile.FirstName;
            this.LastName = profile.LastName;
            this.Department = profile.Department != null ? new DepartmentDTO(profile.Department) : null;
        }
    }
}