namespace Profiles.Domain.Events
{
    public interface IEventBus
    {
        void Publish(IntegrationEvent @event);
    }
}