using System;
using Newtonsoft.Json;

namespace Profiles.Domain.Events
{
    public abstract class IntegrationEvent
    {
        [JsonProperty("timestamp")]
        public DateTime TimeStamp { get => DateTime.Now; }

        [JsonProperty("event")]
        public abstract string EventName { get; }

    }
}