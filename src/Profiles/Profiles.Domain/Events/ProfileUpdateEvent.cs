using Profiles.Domain.ProfilesAggregate;
using Newtonsoft.Json;

namespace Profiles.Domain.Events
{
    public class ProfileUpdatedEvent : IntegrationEvent
    {
        public override string EventName
        {
            get
            {
                return nameof(ProfileUpdatedEvent);
            }
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("departmentId")]
        public int? DepartmentId { get; set; }

        [JsonProperty("departmentTitle")]
        public string DepartmentTitle { get; set; }

        public ProfileUpdatedEvent(Profile profile)
        {
            this.Id = profile.Id;
            this.FirstName = profile.FirstName;
            this.LastName = profile.LastName;
            
            if (profile.Department == null) return;

            this.DepartmentId = profile.Department.Id;
            this.DepartmentTitle = profile.Department.Title;
        }
    }

}