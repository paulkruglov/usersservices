using System.Threading.Tasks;

namespace Profiles.Domain.ProfilesAggregate
{
    public interface IProfilesRepository
    {
        Task SaveProfileAsync(Profile profile);

        Task<Profile> GetByIdAsync(int id);
    }
}