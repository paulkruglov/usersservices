namespace Profiles.Domain.ProfilesAggregate
{
    public class Department
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}