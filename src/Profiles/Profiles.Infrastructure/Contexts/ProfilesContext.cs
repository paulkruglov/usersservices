using Microsoft.EntityFrameworkCore;
using Profiles.Domain.ProfilesAggregate;

namespace Profiles.Infrastructure.Contexts
{
    public class ProfilesContext : DbContext
    {
        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Department> Departments { get; set; }

        public ProfilesContext(DbContextOptions<ProfilesContext> options) : base(options) { }
        
    }
}