using System;
using System.Text;
using Newtonsoft.Json;
using Profiles.Domain.Events;
using RabbitMQ.Client;

namespace Profiles.Infrastructure.RabbitMq
{
    public class RabbitMqEventBus : IEventBus
    {
        private readonly IRabbitMqConnection _connection;

        private const string PROFILE_UPDATED_EVENT = "profile_updated";

        public RabbitMqEventBus(IRabbitMqConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        public void Publish(IntegrationEvent @event)
        {
            if (!this._connection.IsConnected) 
            {
                this._connection.TryConnect();
            }

            var message = JsonConvert.SerializeObject(@event);
            var body = Encoding.UTF8.GetBytes(message);

            using(var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: PROFILE_UPDATED_EVENT, type: "direct", durable: true);

                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2; // persistent

                channel.BasicPublish(
                    exchange: PROFILE_UPDATED_EVENT,
                    routingKey: @event.EventName,

                    basicProperties: properties,
                    body: body);
            }
        }
    }
}