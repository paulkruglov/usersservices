using System;
using RabbitMQ.Client;

namespace Profiles.Infrastructure.RabbitMq
{
    public interface IRabbitMqConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}