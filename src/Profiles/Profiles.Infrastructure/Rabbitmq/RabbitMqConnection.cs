using System;
using RabbitMQ.Client;

namespace Profiles.Infrastructure.RabbitMq
{
    public class RabbitMqConnection : IRabbitMqConnection
    {

        private readonly IConnectionFactory _connectionFactory;
        private IConnection _connection;

        private bool _isDisposed;

        private object _syncObj = new object();

        public RabbitMqConnection(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
        }
        public bool IsConnected => _connection != null && _connection.IsOpen && !_isDisposed;

        public IModel CreateModel()
        {
            return _connection.CreateModel();
        }

        public void Dispose()
        {
            if (this._isDisposed) return;

            this._isDisposed = true;

            try 
            {
                _connection.Dispose();
            }
            catch (Exception ex)
            {
                // TODO log
            }
        }

        public bool TryConnect()
        {
            lock(_syncObj)
            {
                this._connection = _connectionFactory.CreateConnection();

                return this.IsConnected;
            }
        }
    }
}