using Microsoft.Extensions.DependencyInjection;
using Profiles.Domain.Events;
using RabbitMQ.Client;

namespace Profiles.Infrastructure.RabbitMq.Extensions
{
    public static class ServiceCollectionRabbitMqExtension
    {
        public static IServiceCollection AddRabbitMq(this IServiceCollection services, string hostname)
        {
            services.AddScoped<IEventBus, RabbitMqEventBus>();

            services.AddSingleton<IRabbitMqConnection, RabbitMqConnection>( provider => 
            {
                var factory = new ConnectionFactory()
                {
                    HostName = hostname,
                };

                return new RabbitMqConnection(factory);
            });

            return services;
            
        }
    }
}