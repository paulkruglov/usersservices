using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Profiles.Domain.ProfilesAggregate;
using Profiles.Infrastructure.Contexts;

namespace Profiles.Infrastructure.Repositories
{
    public class ProfilesRepository : IProfilesRepository
    {

        private readonly ProfilesContext _context;

        public ProfilesRepository(ProfilesContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task<Profile> GetByIdAsync(int id)
        {
            return _context.Profiles
                .Include(profile => profile.Department)
                .FirstOrDefaultAsync(profile => profile.Id == id);
        }

        public async Task SaveProfileAsync(Profile profile)
        {

            Department department = null;

            if(profile.Department != null)
            {
                department = await _context.Departments.FindAsync(profile.Department.Id);
            }

            var entity = await _context.Profiles.FindAsync(profile.Id);

            _context.Entry(entity).Property("DepartmentId").CurrentValue = department?.Id;
            _context.Entry(entity).Property("DepartmentId").IsModified = true;

            entity.Department = department;

            if (department == null) entity.Department = null;

            entity.FirstName = profile.FirstName;
            entity.LastName = profile.LastName;

            await _context.SaveChangesAsync();
            
        }
    }
}