using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Profiles.Domain.ProfilesAggregate;
using Profiles.Infrastructure.Contexts;
using Profiles.Infrastructure.Repositories;

namespace Profiles.Infrastructure.Repositories.Extensions
{
    public static class RepositoryServiceCollectionExtension
    {
        public static IServiceCollection AddProfilesRepository(this IServiceCollection services)
        {

            services.AddScoped<IProfilesRepository, ProfilesRepository>();

            string connectionString = Environment.GetEnvironmentVariable("POSTGRES_DB_CONNECTION_STRING");

            services.AddDbContext<ProfilesContext>( opts => 
            {
                opts.UseNpgsql(connectionString);
            });

            return services;
        }
    }
}