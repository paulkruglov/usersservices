using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Profiles.Domain.Events;
using Profiles.Domain.Events.Dto;
using Profiles.Domain.ProfilesAggregate;
using Profiles.Infrastructure.Contexts;

namespace Profiles.WebAPI
{
    public class InitialDataService
    {

        private readonly ProfilesContext _context;
        private readonly IEventBus _eventBus;

        public InitialDataService(ProfilesContext context, IEventBus eventBus)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void InitData()
        { 
            this.InitProfiles();
        }

        private void InitProfiles()
        {

             var departments = new List<Department>()
            {
                new Department{ Id = 1, Title="HR" },
                new Department{ Id = 2, Title="Sales" },
                new Department{ Id = 3, Title="IT" },
            };

            var profiles = new List<Profile>()
            {
                new Profile{ Id = 1, FirstName = "Mike", LastName = "Portnoy", Department = departments[0]},
                new Profile{ Id = 2, FirstName = "Lemmy", LastName = "Kilmister"},
                new Profile{ Id = 3, FirstName = "Angus", LastName = "Young", Department = departments[0]},
                new Profile{ Id = 4, FirstName = "Axel", LastName = "Rose", Department = departments[1]},
                new Profile{ Id = 5, FirstName = "Derek", LastName = "Smith", Department = departments[1]},
                new Profile{ Id = 6, FirstName = "Bono", LastName = "Bono"},
                new Profile{ Id = 7, FirstName = "Till", LastName = "Lindemann",Department = departments[2]},
                new Profile{ Id = 8, FirstName = "Joe", LastName = "Satriani", Department = departments[0]},
                new Profile{ Id = 9, FirstName = "Yngwie", LastName = "Malmsteen"},
                new Profile{ Id = 10, FirstName = "Keith", LastName = "Richards",Department = departments[2] }
            };


            _context.AddRange(profiles);

            _context.SaveChanges();

            foreach(var profile in profiles)
            {
                var @event = new ProfileUpdatedEvent(profile);

                _eventBus.Publish(@event);

            }
        }
    }
}


