using System.Threading;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Profiles.Domain.Commands;
using Profiles.Domain.Events;
using Profiles.Infrastructure.Contexts;
using Profiles.Infrastructure.RabbitMq.Extensions;
using Profiles.Infrastructure.Repositories.Extensions;

namespace Profiles.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // NOTE: Строчка ниже использована только в тестовых целях
            // для избеганий ситуаций, когда в docker-compose сервис загружается быстрее,
            // чем запускается база / шина событий
            Thread.Sleep(15000);
            /***/

            services.AddControllers();

            services.AddMediatR(typeof(UpdateProfileCommand).Assembly);

            services.AddProfilesRepository();

            services.AddRabbitMq(Configuration.GetValue<string>("RABBITMQ_BUS_ADDRESS"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();
            app.UseCors( opts =>
            {
                opts.AllowAnyHeader();
                opts.AllowAnyMethod();
                opts.AllowAnyOrigin();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            // NOTE: для обновления схемы в новосозданной в контейнере базе
            UpdateDatabase(app);
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var bus = serviceScope.ServiceProvider.GetService<IEventBus>();
                using (var context = serviceScope.ServiceProvider.GetService<ProfilesContext>())
                {
                    context.Database.Migrate();

                    var initiator = new InitialDataService(context, bus);
                    initiator.InitData();
                }
            }
        }
    }
}
