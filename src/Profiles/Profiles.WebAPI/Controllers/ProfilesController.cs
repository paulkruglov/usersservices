using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Profiles.Domain.Commands;

namespace Profiles.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProfilesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProfilesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateProfile(int id, UpdateProfileCommand command)
        {
            command.Id = id;
            await _mediator.Send(command);

            return Ok();
        }
    }
}