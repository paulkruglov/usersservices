import Vue from 'vue';
import VueRouter from 'vue-router';
import UserList from './views/UsersList.vue'
import UserEdit from './views/UserEdit.vue';

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'users-list',
            component: UserList
        }, 
        {
            path: '/edit',
            name: 'users-edit',
            component: UserEdit,
        },
        {
            path: '*',
            redirect: '/',
        }
    ],
})