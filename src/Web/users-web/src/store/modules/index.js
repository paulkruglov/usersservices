import users from './users';

export default {
    users: {
        namespaced: true,
        ...users,
    }
}