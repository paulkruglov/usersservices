import Axios from 'axios';
const ACCOUNTS_SERVICE_URL = process.env.VUE_APP_ACCOUNT_SERVICE_URL;
const PROFILES_SERVICE_URL = process.env.VUE_APP_PROFILE_SERVICE_URL;
const USERS_SERVICE_URL = process.env.VUE_APP_USERS_SERVICE_URL;

export default {
    'fetch': ({commit}) => {

        let deps = [{id: null, title: ''}];

        Axios.get(`${USERS_SERVICE_URL}/users`).then( ({data}) => {
            commit('SET_USERS', data);
        });

        Axios.get(`${USERS_SERVICE_URL}/departments`).then( ({data}) => {
            deps.push(...data);
            commit('SET_DEPARTMENTS', deps);
        });
        
    },

    'setSearchFilterValue' : ({commit}, value) => {
        commit('SET_FILTER_VALUE', value ? value : '');
    },

    'setDepartmentFilter' : ({commit}, value) => {
        commit('SET_DEPARTMENT_FILTER', value);
    },

    'setUser': ({commit}, value) => {
        commit('SET_SELECTED_USER', value);
    },

    'clearUser': ({commit}) => {
        commit('CLEAR_SELECTED_USER');
    },

    'updateAccount': ({commit}, account) => {
        // todo commit ... what? isLoading mb

        let url = `${ACCOUNTS_SERVICE_URL}/api/v1/accounts/${account.id}`;
        
        let body = {
            name: account.name,
            email: account.email,
        };

        Axios.put(url, body).then( () => {
            commit('UPDATE_USER_ACCOUNT', account);
        }, () => {

        });
        
    },

    'updateProfile': ({commit}, profile) => {

        let url = `${PROFILES_SERVICE_URL}/profiles/${profile.id}`;
        
        let body = {
            firstName: profile.firstName,
            lastName: profile.lastName,
            departmentId: profile.department ? profile.department.id : null,
        };

        Axios.put(url, body).then( () => {
            commit('UPDATE_USER_PROFILE', profile);
        }, () => {

        });

    },
}