export default {
    users: state => state.users,

    searchFilter: state => state.searchFilter,

    filteredUsers: (state, { searchFilter, depatmentFilter }) => {
        let { users } = state;

        if (searchFilter.length != 0) {
            users = users.filter( user => 
                user.name.toLowerCase().includes(searchFilter.toLowerCase())        ||
                user.firstName.toLowerCase().includes(searchFilter.toLowerCase())   ||
                user.lastName.toLowerCase().includes(searchFilter.toLowerCase())    ||
                user.email.toLowerCase().includes(searchFilter.toLowerCase())
                );
        }

        if (depatmentFilter && depatmentFilter.id) {
            users = users.filter( user => {
                if (!user.department) return true;

                return user.department.id == depatmentFilter.id
            });
        }

        return users;
    },

    departments: state => state.departments,

    depatmentFilter: state => state.depatmentFilter,

    selectedUser: state => state.selectedUser,
}