export default {
    SET_USERS (state, usersList) {
        state.users = usersList
    },

    SET_FILTER_VALUE (state, filterValue) {
        state.searchFilter = filterValue
    },

    SET_DEPARTMENTS (state, departments) {
        state.departments = departments
    },

    SET_DEPARTMENT_FILTER (state, department) {
        state.depatmentFilter = department;
    },

    SET_SELECTED_USER (state, user) {
        state.selectedUser = user;
    },

    CLEAR_SELECTED_USER (state) {
        state.selectedUser = null;
    },

    UPDATE_USER_ACCOUNT (state, account) {
        let index = state.users.findIndex( u => u.id == account.id);

        if (index < 0) return;

        state.users[index].name = account.name;
        state.users[index].email = account.email;
    },

    UPDATE_USER_PROFILE (state, profile) {
        let index = state.users.findIndex( u => u.id == profile.id );

        if (index < 0) return;

        state.users[index].firstName = profile.firstName;
        state.users[index].lastName = profile.lastName;
        state.users[index].department = profile.department;
    },
}