namespace Users.Domain.Aggregate
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? DepartmentId { get; set; }

        public string DepartmentTitle { get; set; }

        public User() { }

        public User(Account account)
        {
            this.Id = account.Id;
            this.Name = account.Name;
            this.Email = account.Email;
        }

        public User(Profile profile)
        {
            this.Id = profile.Id;
            this.FirstName = profile.FirstName;
            this.LastName = profile.LastName;
            
            if (profile.Department == null ) return;

            this.DepartmentId = profile.Department.Id;
            this.DepartmentTitle = profile.Department.Title;
        }
    }
}