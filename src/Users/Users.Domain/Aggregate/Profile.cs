namespace Users.Domain.Aggregate
{
    public class Profile
    {
        public int Id { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public Department Department { get; set; }
    }

    public class Department
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public Department(int id, string title)
        {
            this.Id = id;
            this.Title = title;
        }
    }
}