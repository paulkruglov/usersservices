using System.Collections.Generic;
using System.Threading.Tasks;

namespace Users.Domain.Aggregate
{
    public interface IUsersRepository
    {
        Task<IEnumerable<User>> GetAllUsers();

        Task UpdateUserAsync(Account account);

        Task UpdateUserAsync(Profile profile);

        Task<IEnumerable<Department>> GetDepartments();

    }
}