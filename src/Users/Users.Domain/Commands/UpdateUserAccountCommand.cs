using MediatR;

namespace Users.Domain.Commands
{
    public class UpdateUserAccountCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}