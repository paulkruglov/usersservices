using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Users.Domain.Aggregate;

namespace Users.Domain.Commands
{
    public class UpdateUserAccountCommandHandler : IRequestHandler<UpdateUserAccountCommand>
    {
        private readonly IUsersRepository _usersRepository;

        public UpdateUserAccountCommandHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
        }

        public async Task<Unit> Handle(UpdateUserAccountCommand request, CancellationToken cancellationToken)
        {
            var account = new Account
            {
                Id = request.Id,
                Email = request.Email,
                Name = request.Name,
            };
            await _usersRepository.UpdateUserAsync(account);

            return new Unit();

        }
    }
}