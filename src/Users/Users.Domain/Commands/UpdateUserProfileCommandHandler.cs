using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Users.Domain.Aggregate;

namespace Users.Domain.Commands
{
    public class UpdateUserProfileCommandHandler : IRequestHandler<UpdateUserProfileCommand>
    {
        private readonly IUsersRepository _usersRepository;

        public UpdateUserProfileCommandHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
        }

        public async Task<Unit> Handle(UpdateUserProfileCommand request, CancellationToken cancellationToken)
        {
            var profile = new Profile
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            if (request.DepartmentId.HasValue)
            {
                profile.Department = new Department(request.DepartmentId.Value, request.DepartmentTitle);
            }

            await _usersRepository.UpdateUserAsync(profile);

            return new Unit();
        }
    }
}