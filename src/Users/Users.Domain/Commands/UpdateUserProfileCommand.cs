using MediatR;

namespace Users.Domain.Commands
{
    public class UpdateUserProfileCommand : IRequest
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? DepartmentId { get; set; }

        public string DepartmentTitle { get; set; }
    }
}