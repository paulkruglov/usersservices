using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Users.Domain.Aggregate;
using Users.Domain.Queries.Dtos;

namespace Users.Domain.Queries
{
    public class GetAllDepartmentsQueryHandler : IRequestHandler<GetAllDepartmentsQuery, IEnumerable<DepartmentDto>>
    {

        private readonly IUsersRepository _usersRepository;

        public GetAllDepartmentsQueryHandler(IUsersRepository repository)
        {
            _usersRepository = repository;
        }

        public async Task<IEnumerable<DepartmentDto>> Handle(GetAllDepartmentsQuery request, CancellationToken cancellationToken)
        {
            var departments = await _usersRepository.GetDepartments();

            var departmentsDtos = departments.Select(department => new DepartmentDto{ Id = department.Id, Title = department.Title });

            return departmentsDtos;
        }
    }
}