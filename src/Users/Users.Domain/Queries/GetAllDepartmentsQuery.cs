using System.Collections.Generic;
using MediatR;
using Users.Domain.Queries.Dtos;

namespace Users.Domain.Queries
{
    public class GetAllDepartmentsQuery : IRequest<IEnumerable<DepartmentDto>>
    {
        
    }
}