using System.Collections.Generic;
using MediatR;
using Users.Domain.Queries.Dtos;

namespace Users.Domain.Queries
{
    public class GetAllUsersQuery : IRequest<IEnumerable<UserDto>>
    {
    }
}