namespace Users.Domain.Queries.Dtos
{
    public class DepartmentDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}