using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Users.Domain.Aggregate;
using Users.Domain.Queries.Dtos;

namespace Users.Domain.Queries
{
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, IEnumerable<UserDto>>
    {

        private readonly IUsersRepository _usersRepository;

        public GetAllUsersQueryHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
        }

        public async Task<IEnumerable<UserDto>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _usersRepository.GetAllUsers();

            var usersDtos = users.Select(user =>
            {
                var dto = new UserDto
                {
                    Id = user.Id,
                    Name = user.Name,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                };

                if (user.DepartmentId.HasValue)
                {
                    dto.Department = new DepartmentDto{ Id = user.DepartmentId.Value, Title = user.DepartmentTitle };
                }
                

                return dto;
            });

            return usersDtos.AsEnumerable();
        }
    }
}