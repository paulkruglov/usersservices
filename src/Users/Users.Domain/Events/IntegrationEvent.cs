namespace Users.Domain.Events
{
    public abstract class IntegrationEvent
    {
        public abstract string EventName { get; }
    }
}