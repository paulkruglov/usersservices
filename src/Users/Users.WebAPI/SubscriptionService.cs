using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Users.Infrastructure.Events;
using Users.Infrastructure.RabbitMq;

namespace Users.WebAPI
{
    public class SubscriptionService : IHostedService
    {
        private readonly RabbitMqEventBus _eventBus;

        public SubscriptionService(RabbitMqEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _eventBus.SubscribeUpdateAccount();
            await _eventBus.SubscribeUpdateProfile();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            // todo
            return Task.CompletedTask;
        }
    }
}