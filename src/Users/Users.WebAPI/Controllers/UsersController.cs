using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Users.Domain.Commands;
using Users.Domain.Queries;

namespace Users.WebAPI.Controllers
{
    [ApiController]
    [Route("api/v1/users")]
    public class UsersControllers : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersControllers(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _mediator.Send(new GetAllUsersQuery());

            return Ok(users);
        }

        [HttpGet]
        [Route("~/api/v1/departments")]
        public async Task<IActionResult> GetDepartments() 
        {
            var departments = await _mediator.Send(new GetAllDepartmentsQuery());

            return Ok(departments);
        }

    }
}