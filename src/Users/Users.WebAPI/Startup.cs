using System;
using System.Threading;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using Users.Domain.Commands;
using Users.Infrastructure.Contexts;
using Users.Infrastructure.Events;
using Users.Infrastructure.Extensions;
using Users.Infrastructure.RabbitMq;

namespace Users.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // NOTE: ТОЛЬКО В ЦЕЛЯХ ТЕСТИРОВАНИЯ
            // Потом поискать решение в докер/хелсчеках
            Thread.Sleep(10000);

            services.AddControllers();

            services.AddMediatR(typeof(UpdateUserProfileCommand).Assembly);

            services.AddPostgresRepository(Configuration.GetValue<string>("POSTGRES_DB_CONNECTION_STRING"));

            services.AddSingleton<RabbitMqEventBus>();

            services.AddSingleton<IRabbitMqConnection, RabbitMqConnection>(provider =>
           {
               var factory = new ConnectionFactory()
               {
                   HostName = Configuration.GetValue<string>("RABBITMQ_BUS_ADDRESS"),
               };

               return new RabbitMqConnection(factory);
           });

            services.AddHostedService<SubscriptionService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();
            app.UseCors( opts =>
            {
                opts.AllowAnyHeader();
                opts.AllowAnyMethod();
                opts.AllowAnyOrigin();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            UpdateDatabase(app);
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                // var bus = serviceScope.ServiceProvider.GetService<IEventBus>();
                using (var context = serviceScope.ServiceProvider.GetService<UsersContext>())
                {
                    try
                    {
                        context.Database.Migrate();
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
        }
    }
}
