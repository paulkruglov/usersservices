using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Users.Domain.Aggregate;
using Users.Infrastructure.Contexts;
using Users.Infrastructure.Repositories;

namespace Users.Infrastructure.Extensions
{
    public static class IServiceCollectionRepositoryExtension
    {
        public static IServiceCollection AddPostgresRepository(this IServiceCollection services, string connectionString)
        {
            services.AddTransient<IUsersRepository, UsersRepository>();

            services.AddDbContext<UsersContext>(provider =>
            {
                provider.UseNpgsql(connectionString);
            });

            return services;
        }
    }
}