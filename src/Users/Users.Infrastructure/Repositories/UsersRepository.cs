using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Users.Domain.Aggregate;
using Users.Infrastructure.Contexts;

namespace Users.Infrastructure.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly UsersContext _context;

        public UsersRepository(UsersContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            var users = await _context.Users.ToListAsync();
            return users.AsEnumerable();
        }

        public async Task UpdateUserAsync(Account account)
        {

            try
            {
                await this.AddUserAsync(account);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                {
                    await this.UpdateUser(account);
                    return;
                }
                System.Console.WriteLine(ex.ToString());
            }
        }

        public async Task UpdateUserAsync(Profile profile)
        {
            try
            {
                await this.AddUserAsync(profile);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                {
                    await this.UpdateUser(profile);
                    return;
                }

                System.Console.WriteLine(ex.ToString());
            }
        }

        private async Task AddUserAsync(Account account)
        {
            var user = new User(account);
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        private async Task AddUserAsync(Profile profile)
        {
            var user = new User(profile);
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        private async Task UpdateUser(Account account)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == account.Id);

            if (user != null)
            {
                _context.Users.Attach(user);

                var entry = _context.Entry(user);
                entry.Property("Name").IsModified = true;
                entry.Property("Email").IsModified = true;

                user.Name = account.Name;
                user.Email = account.Email;

                await _context.SaveChangesAsync();
            }

        }

        private async Task UpdateUser(Profile profile)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == profile.Id);

            if (user == null) return;

            _context.Users.Attach(user);

            var entry = _context.Entry(user);
            entry.Property("FirstName").IsModified = true;
            entry.Property("LastName").IsModified = true;
            entry.Property("DepartmentId").IsModified = true;
            entry.Property("DepartmentTitle").IsModified = true;

            user.FirstName = profile.FirstName;
            user.LastName = profile.LastName;
            user.DepartmentId = profile.Department?.Id;
            user.DepartmentTitle = profile.Department?.Title;

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Department>> GetDepartments()
        {
            var departments = _context.Users.Where(user => user.DepartmentId.HasValue)
                                                    .Select(user => new Department(user.DepartmentId.Value, user.DepartmentTitle))
                                                    .Distinct();

            return departments;
        }
    }
}