using System.Threading;
using System.Threading.Tasks;
using System;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json;
using Users.Domain.Commands;
using System.Text;

namespace Users.Infrastructure.Events
{
    public class RabbitMqEventBus
    {
        private readonly IRabbitMqConnection _connection;
        private readonly IServiceProvider _provider;

        private SemaphoreSlim _semaphore;

        public RabbitMqEventBus(IRabbitMqConnection connection, IServiceProvider provider)
        {
            _connection = connection;
            _provider = provider;
        }

        public async Task SubscribeUpdateAccount()
        {
            var channel = _connection.CreateModel();

            channel.ExchangeDeclare(exchange: "account_updated", type: "direct", durable: true);
            channel.QueueDeclare("account_updated", false, false, false);
            channel.QueueBind("account_updated", "account_updated", "AccountUpdatedEvent");

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += async (msg, args) =>
            {
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                using (var scope = _provider.CreateScope())
                {
                    var mediatr = scope.ServiceProvider.GetRequiredService<IMediator>();

                    var command = JsonConvert.DeserializeObject<UpdateUserAccountCommand>(message);

                    try
                    {
                        await mediatr.Send(command);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                    }

                }

            };
            channel.BasicConsume(queue: "account_updated",
                                                    autoAck: true,
                                                    consumer: consumer);

        }

        public async Task SubscribeUpdateProfile()
        {
            var channel = _connection.CreateModel();

            channel.ExchangeDeclare(exchange: "profile_updated", type: "direct", durable: true);
            channel.QueueDeclare("profile_updated", false, false, false);
            channel.QueueBind("profile_updated", "profile_updated", "ProfileUpdatedEvent");

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += async (msg, args) =>
            {
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);


                using (var scope = _provider.CreateScope())
                {
                    var mediatr = scope.ServiceProvider.GetRequiredService<IMediator>();

                    UpdateUserProfileCommand command;

                    try
                    {
                        command = JsonConvert.DeserializeObject<UpdateUserProfileCommand>(message);
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    try
                    {
                        await mediatr.Send(command);
                    }
                    catch (Exception)
                    {

                    }
                }
            };
            channel.BasicConsume(queue: "profile_updated",

                                                    autoAck: true,
                                                    consumer: consumer);

        }
    }
}