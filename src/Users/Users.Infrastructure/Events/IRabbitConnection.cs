using System;
using RabbitMQ.Client;

namespace Users.Infrastructure.Events
{
    public interface IRabbitMqConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}