using Microsoft.EntityFrameworkCore;
using Users.Domain.Aggregate;

namespace Users.Infrastructure.Contexts
{
    public class UsersContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public UsersContext(DbContextOptions<UsersContext> options) : base(options) { }
    }
}