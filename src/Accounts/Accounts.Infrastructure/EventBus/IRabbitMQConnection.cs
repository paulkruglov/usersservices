using System;
using RabbitMQ.Client;

namespace Accounts.Infrastructure.EventBus
{
    public interface IRabbitMQConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}