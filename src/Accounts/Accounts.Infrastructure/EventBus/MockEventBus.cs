using Accounts.Domain.Events;

namespace Accounts.Infrastructure.EventBus
{
    public class MockEventBus : IEventBus
    {
        public void Publish(IntegrationEvent @event)
        {
            string eventName = @event.GetType().Name;

            System.Console.WriteLine(eventName);

        }
    }
}