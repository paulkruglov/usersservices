using System;
using System.Text;
using System.Threading.Tasks;
using Accounts.Domain.Events;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Accounts.Infrastructure.EventBus
{
    public class RabbitMQEventBus : IEventBus
    {
        private readonly IRabbitMQConnection _connection;
        private const string ACCOUNT_UPDATED = "account_updated";

        public RabbitMQEventBus(IRabbitMQConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        public void Publish(IntegrationEvent @event)
        {
            if (!_connection.IsConnected) 
            {
                _connection.TryConnect();
            }

            var eventName = @event.GetType().Name;

            using(var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: ACCOUNT_UPDATED, type: "direct", durable: true);

                var message = JsonConvert.SerializeObject(@event);
                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2; // persistent

                channel.BasicPublish(
                    exchange: ACCOUNT_UPDATED,
                    routingKey: eventName,

                    basicProperties: properties,
                    body: body);

            }

        }
    }
}