using System.IO;
using System;
using RabbitMQ.Client;

namespace Accounts.Infrastructure.EventBus
{
    public class RabbitMQConnection : IRabbitMQConnection
    {
        private readonly IConnectionFactory _connectionFactory;

        private IConnection _connection;
        private bool _isDisposed;

        private object _sync_obj = new object();

        public RabbitMQConnection(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
        }

        public bool IsConnected
        {
            get
            {
                return _connection != null && _connection.IsOpen && !_isDisposed;
            }
        }

        public IModel CreateModel()
        {
            return this._connection.CreateModel();
        }

        public bool TryConnect()
        {
            lock (_sync_obj)
            {

                this._connection = _connectionFactory.CreateConnection();

                if (this.IsConnected)
                {
                    return true;
                }

                return false;
            }
        }

        public void Dispose()
        {
            if (this._isDisposed) return;

            this._isDisposed = true;

            try 
            {
                _connection.Dispose();
            }
            catch(IOException ex)
            {
                // TODO log
            }
        }
    }
}