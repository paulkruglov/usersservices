using System.Threading.Tasks;
using Accounts.Domain.AccountsAggregate;

namespace Accounts.Infrastructure.Repositories
{
    public class AccountsMockRepository : IAccountsRepository
    {
        public Task SaveAccountAsync(Account account)
        {

            System.Console.WriteLine($"save {account.Id} {account.Name} {account.Email}");

            return Task.CompletedTask;
        }
    }
}