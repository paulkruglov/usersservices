using System;
using System.Linq;
using System.Threading.Tasks;
using Accounts.Domain.AccountsAggregate;
using Accounts.Domain.Exceptions;
using Accounts.Infrastructure.Contexts;

namespace Accounts.Infrastructure.Repositories
{
    public class AccountsRepository : IAccountsRepository
    {
        private readonly AccountsContext _context;

        public AccountsRepository(AccountsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task SaveAccountAsync(Account account)
        {

            var entry = _context.Accounts.FirstOrDefault(entity => entity.Id == account.Id);

            if (entry == null)
            {
                throw new AccountNotFoundException(account);
            }

            entry.Email = account.Email;
            entry.Name = account.Name;

            _context.Update(entry);

            await _context.SaveChangesAsync();

        }
    }
}