using Accounts.Domain.Behaviours;
using Accounts.Domain.Commands;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Accounts.WebAPI.Extensions 
{
    public static class IServiceCollectionMediatrExtension
    {
        public static IServiceCollection AddMediatrPipeline(this IServiceCollection services)
        {
            services.AddMediatR(typeof(UpdateAccountCommandHandler));

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            
            services.AddValidatorsFromAssemblyContaining(typeof(ValidationBehaviour<,>));

            return services;
        }
    }
}