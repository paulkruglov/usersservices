using System;
using Accounts.Domain.AccountsAggregate;
using Accounts.Domain.Behaviours;
using Accounts.Domain.Commands;
using Accounts.Infrastructure.Contexts;
using Accounts.Infrastructure.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Accounts.WebAPI.Extensions
{
    public static class IServiceCollectionRepositoryExtension
    {
        public static IServiceCollection AddAccountRepository(this IServiceCollection services)
        {

            services.AddScoped<IAccountsRepository, AccountsRepository>();

            string connectionString = Environment.GetEnvironmentVariable("POSTGRES_DB_CONNECTION_STRING");

            services.AddDbContext<AccountsContext>(optionsAction =>
            {
                optionsAction.UseNpgsql(connectionString);
            });

            return services;
        }
    }
}