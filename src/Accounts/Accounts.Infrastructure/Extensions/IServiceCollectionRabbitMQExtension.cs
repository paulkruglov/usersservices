using Accounts.Domain.Events;
using Accounts.Infrastructure.EventBus;
using Microsoft.Extensions.DependencyInjection;

namespace Accounts.WebAPI.Extensions 
{
    public static class IServiceCollectionRabbitMQExtension
    {
        public static IServiceCollection AddRabbitMQ(this IServiceCollection services, string hostname)
        {      
            services.AddScoped<IEventBus, RabbitMQEventBus>();

            services.AddSingleton<IRabbitMQConnection>( provider =>
            {
                var factory = new RabbitMQ.Client.ConnectionFactory()
                {
                    HostName = hostname,
                };

                return new RabbitMQConnection(factory);
            });     

            return services;
        }
    }
}