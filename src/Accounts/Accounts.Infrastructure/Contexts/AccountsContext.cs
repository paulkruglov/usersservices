using Accounts.Domain.AccountsAggregate;
using Microsoft.EntityFrameworkCore;

namespace Accounts.Infrastructure.Contexts
{
    public class AccountsContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }

        public AccountsContext(DbContextOptions<AccountsContext> options) : base(options) { }
    }
}