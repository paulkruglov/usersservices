using System.Threading;
using Accounts.Domain.Events;
using Accounts.Infrastructure.Contexts;
using Accounts.WebAPI.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Accounts.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // NOTE: Строчка ниже использована только в тестовых целях
            // для избежания ситуаций, когда в docker-compose сервис загружается быстрее,
            // чем запускается база / шина событий
            Thread.Sleep(15000);

            services.AddControllers();

            services.AddMediatrPipeline();

            services.AddRabbitMQ(Configuration.GetValue<string>("RABBITMQ_BUS_ADDRESS"));

            services.AddAccountRepository();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler("/errors");
            app.UseCors( opts =>
            {
                opts.AllowAnyHeader();
                opts.AllowAnyMethod();
                opts.AllowAnyOrigin();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            UpdateDatabase(app);

        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var bus = serviceScope.ServiceProvider.GetService<IEventBus>();
                using (var context = serviceScope.ServiceProvider.GetService<AccountsContext>())
                {
                    context.Database.Migrate();

                    var initiator = new InitialDataService(context, bus);
                    initiator.Initialize();
                }
            }
        }
    }
}
