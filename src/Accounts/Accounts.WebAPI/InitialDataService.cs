using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Accounts.Domain.AccountsAggregate;
using Accounts.Domain.Events;
using Accounts.Infrastructure.Contexts;
using Microsoft.Extensions.Hosting;

namespace Accounts.WebAPI
{

    /// <summary>
    /// Класс сервиса инициализации первичных данных
    /// </summary>
    public class InitialDataService
    {

        private readonly AccountsContext _context;
        private readonly IEventBus _eventBus;

        public InitialDataService(AccountsContext context, IEventBus eventBus)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));

        }

        public void Initialize()
        {

            var accounts = new List<Account>()
            {
                new Account(1, "mikeP", "mike@p.com"),
                new Account(2, "lemmy", "kil@mister.mh"),
                new Account(3, "angus", "ac@dc.gov"),
                new Account(4, "axl", "gr@mail.me"),
                new Account(5, "derek", "roddy@sher.ag"),
                new Account(6, "bono", "bo@no.zc"),
                new Account(7, "till", "lindemann@b.de"),
                new Account(8, "joes", "satri@ni.mail"),
                new Account(9, "malmsteen", "pettrucci@best.zss"),
                new Account(10, "keith", "rolling@stones.qwe")
            };

            _context.AddRange(accounts);

            _context.SaveChanges();

            foreach(var account in accounts)
            {
                _eventBus.Publish(new AccountUpdatedEvent(account));
            }

        }

    }
}