using System.Net;
using System.Threading.Tasks;
using Accounts.Domain.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace Accounts.WebAPI.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("errors")]
    public class ErrorsController : ControllerBase
    {
        public JsonResult ErrorResponse()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;
            var code = HttpStatusCode.InternalServerError;

            if (exception is FluentValidation.ValidationException)  code = HttpStatusCode.BadRequest;
            if (exception is AccountNotFoundException)              code = HttpStatusCode.NotFound;

            Response.StatusCode = (int) code;

            return new JsonResult(new { statusCode = code, title = exception.Message, details = exception.ToString()});
        }
    }
}