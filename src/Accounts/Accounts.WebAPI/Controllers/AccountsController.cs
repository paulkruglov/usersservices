using System;
using System.Threading.Tasks;
using Accounts.Domain.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Accounts.WebAPI.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateAccount(int id, UpdateAccountCommand command)
        {
            command.Id = id;

            await _mediator.Send(command);
            return Ok();    // TODO change to Accepted or created
        }
    }
}