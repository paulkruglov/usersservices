using System.Text.RegularExpressions;
using Accounts.Domain.Commands;
using FluentValidation;

namespace Accounts.Domain.Validations
{
    public class UpdateAccountCommandValidator : AbstractValidator<UpdateAccountCommand>
    {

        public UpdateAccountCommandValidator()
        {
            RuleFor(c => c.Name).Cascade(CascadeMode.Stop)
                .NotNull().WithMessage("не указано имя")
                .NotEmpty().WithMessage("не указано имя");

            RuleFor(c => c.Email).Cascade(CascadeMode.Stop)
                .NotNull().WithMessage("не указан e-mail")
                .NotEmpty().WithMessage("не указан e-mail")
                .EmailAddress().WithMessage("невалидный e-mail");

        }
    }
}