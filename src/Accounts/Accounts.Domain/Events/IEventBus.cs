namespace Accounts.Domain.Events
{
    public interface IEventBus
    {
        void Publish(IntegrationEvent @event);
    }
}