using System;
using Newtonsoft.Json;

namespace Accounts.Domain.Events
{
    public class IntegrationEvent
    {
        [JsonProperty("guid")]
        public Guid Guid { get; private set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; private set; }

        public IntegrationEvent()
        {
            this.Guid = Guid.NewGuid();
            this.Timestamp = DateTime.Now;
        }

        public IntegrationEvent(Guid Guid, DateTime time)
        {
            this.Guid = Guid;
            this.Timestamp = time;
        }
    }
}