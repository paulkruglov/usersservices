using System;
using Accounts.Domain.AccountsAggregate;
using Newtonsoft.Json;

namespace Accounts.Domain.Events
{
    public class AccountUpdatedEvent : IntegrationEvent
    {
        [JsonProperty("id")]
        public int Id { get; }

        [JsonProperty("name")]
        public string Name { get; }

        [JsonProperty("email")]
        public string Email { get; }

        public AccountUpdatedEvent(int id, string name, string email) : base()
        {
            this.Id = id;
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
        }

        public AccountUpdatedEvent(Account account) : this(account.Id, account.Name, account.Email) { }
    }
}