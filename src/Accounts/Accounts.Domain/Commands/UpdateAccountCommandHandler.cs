using System;
using System.Threading;
using System.Threading.Tasks;
using Accounts.Domain.AccountsAggregate;
using Accounts.Domain.Commands;
using Accounts.Domain.Events;
using MediatR;

namespace Accounts.Domain.Commands
{
    public class UpdateAccountCommandHandler : IRequestHandler<UpdateAccountCommand>
    {

        private readonly IAccountsRepository _repository;
        private readonly IEventBus _eventBus;

        public UpdateAccountCommandHandler(IAccountsRepository repository, IEventBus eventBus)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public async Task<Unit> Handle(UpdateAccountCommand request, CancellationToken cancellationToken)
        {
            Account account = new Account(request.Id, request.Name, request.Email);

            await _repository.SaveAccountAsync(account);

            _eventBus.Publish(new AccountUpdatedEvent(account));

            return new Unit();
        }
    }
}