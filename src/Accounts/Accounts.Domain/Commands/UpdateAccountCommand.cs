using MediatR;

namespace Accounts.Domain.Commands
{
    public class UpdateAccountCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}