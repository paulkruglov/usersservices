using System;
using Accounts.Domain.AccountsAggregate;

namespace Accounts.Domain.Exceptions
{
    public class AccountNotFoundException : Exception
    {
        public AccountNotFoundException(Account account) : base($"account with {account.Id} doesn't exist") { }
    }
}