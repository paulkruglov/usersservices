using System.Threading.Tasks;

namespace Accounts.Domain.AccountsAggregate
{
    public interface IAccountsRepository
    {
        Task SaveAccountAsync(Account account);
    }
}