using System;
using Accounts.Domain.SeedWork;

namespace Accounts.Domain.AccountsAggregate
{
    public class Account : Entity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public Account() { }

        public Account(int id, string name, string email)
        {
            base.Id = id;
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
        }
    }
}